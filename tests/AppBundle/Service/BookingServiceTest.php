<?php

/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace Test\Service;

use AppBundle\Entity\Commande;
use AppBundle\Entity\Ticket;
use AppBundle\Service\BookingService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BookingServiceTest extends KernelTestCase
{
    /**
     * @var BookingService
     */
    private $service;


    protected function setUp()
    {
        self::bootKernel();
        $this->service = self::$kernel->getContainer()->get('app.booking');
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->service = null;
    }

    public function testCalculAge()
    {
        $ticket = new Ticket();
        $commande = new Commande();
        $ticket->setDateNaissance(new \DateTime('1984-08-10'));
        $commande->setDateVisit(new \DateTime('2017-10-17'));

        $this->assertEquals(33, $this->service->calculAge($ticket->getDateNaissance(), $commande->getDateVisit()));

    }

    public function testPrixAdultNoReduction()
    {
        $ticket = new Ticket();
        $age = 25;
        $ticket->setReduction(false);

        $this->assertEquals('16', $this->service->priceTicket($age, $ticket->getReduction()));
    }

    public function testPrixAdultReduction()
    {
        $age = 25;
        $reduction = true;

        $this->assertEquals('10', $this->service->priceTicket($age, $reduction));
    }

    public function testPrixEnfantReduction()
    {
        $age = 4;
        $reduction = true;

        $this->assertEquals('8', $this->service->priceTicket($age, $reduction));
    }
}