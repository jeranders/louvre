<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Entity;


use PHPUnit\Framework\TestCase;

class CommandeTest extends TestCase
{
    public function testCommande()
    {
        $commande = new Commande();

        $commande->setLastName('Brechoire');
        $commande->setFirstName('Jérôme');
        $commande->setEmail('brechoire.j@gmail.com');
        $commande->setNumCommande('louvre_com596c99f58b222');

        $this->assertEquals('Brechoire', $commande->getLastName());
        $this->assertEquals('Jérôme', $commande->getFirstName());
        $this->assertEquals('brechoire.j@gmail.com', $commande->getEmail());
        $this->assertEquals('louvre_com596c99f58b222', $commande->getNumCommande());
    }
}