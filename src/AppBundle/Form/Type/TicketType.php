<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Ticket;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class TicketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, array(
                'label' => 'Nom :',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'constraints' => array(
                    new NotBlank(),
                    new NotNull(),
                    new Length(
                        array(
                            'min' => 3,
                            'max' => 100,
                            'minMessage' => 'Le nom doit contenir au moins 3 caractères !',
                            'maxMessage' => 'Le nom ne doit pas contenir plus de 100 caractères !',
                        )
                    )
                )
            ))
            ->add('prenom', TextType::class, array(
                'label' => 'Prénom :',
                'attr' => array(
                  'class' => 'form-control'
                ),
                'constraints' => array(
                    new NotBlank(),
                    new NotNull(),
                    new Length(
                        array(
                            'min' => 3,
                            'max' => 100,
                            'minMessage' => 'Le prénom doit contenir au moins 3 caractères !',
                            'maxMessage' => 'Le prénom ne doit pas contenir plus de 100 caractères !',
                        )
                    )
                )
            ))
            ->add('dateNaissance', DateType::class, array(
                'label' => 'Date de naissance :',
                'placeholder' => 'Exemple : 10-08-1984',
                'attr' => array(
                    'class' => 'form-control format-date'
                ),
                'constraints' => array(
                    new NotBlank(),
                ),
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'html5' => false,
            ))
            ->add('pays', CountryType::class, array(
                'label' => 'Votre pays :',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'preferred_choices' => array('FR'),
            ))
            ->add('reduction', ChoiceType::class, array(
                'label' => 'Réduction :',
                'attr' => array(
                    'class' => 'form-control reduction'
                ),
                'choices' => array(
                    'Non' => false,
                    'Oui' => true,
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Ticket::class,
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_ticket_type';
    }
}
