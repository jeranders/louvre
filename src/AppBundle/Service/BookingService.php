<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 24/05/2017
 * Heure: 09:16
 */

namespace AppBundle\Service;

// Entity
use AppBundle\Entity\Commande;
use AppBundle\Entity\Ticket;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Form\FormFactory;
use AppBundle\Form\Type\CommandeType;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class BookingService
{
    /**
     * @var EntityManager
     */
    private $doctrine;
    /**
     * @var FormFactory
     */
    private $form;

    /**
     * @var int
     */
    private $total;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var TwigEngine
     */
    private $template;

    /**
     * BookingService constructor.
     * @param EntityManager $doctrine
     * @param FormFactory $form
     * @param Session $session
     * @param \Swift_Mailer $mailer
     * @param TwigEngine $template
     */
    public function __construct(EntityManager $doctrine,
                                FormFactory $form,
                                Session $session,
                                \Swift_Mailer $mailer,
                                TwigEngine $template)
    {
        $this->doctrine = $doctrine;
        $this->form = $form;
        $this->session = $session;
        $this->mailer = $mailer;
        $this->template = $template;
    }

    /**
     * Formulaire de commande
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function commande(Request $request)
    {
        $commande = new Commande();
        $form = $this->form->create(CommandeType::class, $commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
            $this->session->set('commande', $data);
            $this->setOrder($commande);

            $this->doctrine->persist($commande);
            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function validation(Request $request)
    {
        $recap = $this->session->get('commande');
        $idCommande = $this->session->get('commande')->getId();
        $commande = $this->doctrine->getRepository('AppBundle:Commande')->findOneBy(array('id'=> $idCommande));

        \Stripe\Stripe::setApiKey('sk_test_RvxKHPKgv2df5RnPSltVwIa8');

        $token = $request->request->get('stripeToken');
        $total = $this->session->get('commande')->getPrix();

        try {
            \Stripe\Charge::create(array(
                'amount' => $total * 100, 'currency' => 'eur', 'source' => $token,
                'description' => 'Paiement Stripe - Louvre'
            ));

            $commande->setValide(true);
            $this->doctrine->persist($commande);
            $this->doctrine->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('Confirmation de réservation')
                ->setFrom('brechoire.j@gmail.com')
                ->setTo($commande->getEmail())
                ->setBody(
                    $this->template->render('Email/email.html.twig', array(
                        'commande' => $recap,
                    )),
                    'text/html'
                )
            ;
            $this->mailer->send($message);

            $response = new RedirectResponse('validation');
            $response->send();

        } catch(\Stripe\Error\Card $e) {

            $this->session->getFlashBag()->add("error",'Une erreur s\'est produite. Veuillez essayer à nouveau');

            return $this->redirectToRoute('recap');
        }

    }

    /**
     * @param Commande $commande
     * @return Commande
     */
    public function setOrder(Commande $commande)
    {
        $tickets = $commande->getTickets();

        foreach ($tickets as $ticket)
        {
            $age = $this->calculAge($ticket->getDateNaissance(), $commande->getDateVisit());
            $price = $this->priceTicket($age, $ticket->getReduction());

            $ticket->setTarif($price);
            $this->total += $ticket->getTarif();

            $commande->setPrix($this->total);

            $ticket->setCommande($commande);
        }
        return $commande;
    }

    /**
     * Calcul de l'age
     *
     * @param $dateNaisance
     * @param $dateVisite
     * @return int
     */
    public function calculAge($dateNaisance, $dateVisite)
    {
        return date_diff($dateNaisance, $dateVisite)->y;
    }

    /**
     * Calcul du tarif
     *
     * Un tarif « normal » à partir de 12 ans à 16 €
     * Un tarif « enfant » à partir de 4 ans et jusqu’à 12 ans, à 8 € (l’entrée est gratuite pour les enfants de moins de 4 ans)
     * Un tarif « senior » à partir de 60 ans pour 12 €
     * Un tarif « réduit » de 10 € accordé dans certaines conditions (étudiant, employé du musée, d’un service du Ministère de la Culture, militaire…)
     *
     * @param $age
     * @param $reduction
     * @return int
     */
    public function priceTicket($age, $reduction)
    {
        switch ($age) {
            case $age >= 4 && $age < 12:
                $tarif = 8;
                break;
            case $age < 4:
                $tarif = 0;
                break;
            case $age >= 60:
                $tarif = 12;
                break;
            case $reduction:
                $tarif = 10;
                break;
            default:
                $tarif = 16;
                break;
        }
        return $tarif;
    }

    /**
     * Suppression de la session de la commande
     *
     * @param Request $request
     * @return Request
     */
    public function unsetSession(Request $request)
    {
        if ($request->hasSession()){
            $request->getSession()->remove('commande');
        }
        return $request;
    }
}