<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 29/05/2017
 * Heure: 15:18
 */

namespace AppBundle\Validators;


use Symfony\Component\Validator\Constraint;

/**
 * Class DayClose
 * @package AppBundle\Validators
 * @Annotation
 */
class DayClose extends Constraint
{
    public $message = "Il n'est pas possible de réserver pour ce jour";

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}