<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 30/05/2017
 * Heure: 06:54
 */

namespace AppBundle\Validators;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class CountTicketValidator
 * @package AppBundle\Validators
 */
class CountTicketValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $doctrine;

    /**
     * CountTicketValidator constructor.
     * @param EntityManagerInterface $doctrine
     */
    public function __construct(EntityManagerInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if(!$value){
            return;
        }

        $daysVisit = $this->doctrine
            ->getRepository('AppBundle:Commande')
            ->findBy(array('dateVisit' => $value))
        ;

        if (empty($daysVisit)) {
            return;
        }

        $nbCommande = 0;

        foreach ($daysVisit as $day)
        {
            $nbTickets = count($day->getTickets());
            $nbCommande += $nbTickets;
        }

        if ($nbCommande > 1000) {
            $this->context->addViolation($constraint->message);
        }
    }
}