<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 30/05/2017
 * Heure: 07:26
 */

namespace AppBundle\Validators;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class HolidayValidator
 * @package AppBundle\Validators
 */
class HolidayValidator extends ConstraintValidator
{
    private $closeHoliday = ['01/05', '01/11', '25/12'];

    public function validate($value, Constraint $constraint)
    {
        if(!$value){
            return;
        }

        $dayVisit = date('d/m', $value->getTimestamp());
        $closeHoliday = $this->CloseHoliday();

        foreach ($closeHoliday as $holiday)
        {
            if ($dayVisit == $holiday)
            {
                $this->context->addViolation($constraint->message);
            }
        }
    }
    public function CloseHoliday() {
        return $this->closeHoliday;
    }
}