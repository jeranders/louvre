<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 29/05/2017
 * Heure: 15:19
 */

namespace AppBundle\Validators;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DayCloseValidator extends ConstraintValidator
{
    // Tableau dans la cas ou d'autres jours seront ajoutés
    private $close = 'Tue';

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if(!$value){
            return;
        }

        //foreach ($close as $closeDay)
      //  {

        $dayVisit = date('D', $value->getTimestamp());
        $close = $this->getClose();

        if ($dayVisit == $close)
        {
            $this->context->addViolation($constraint->message);
        }


        //}
    }
    public function getClose() {
        return $this->close;
    }
}