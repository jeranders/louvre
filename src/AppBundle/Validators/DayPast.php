<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 30/05/2017
 * Heure: 07:29
 */

namespace AppBundle\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * Class DayPast
 * @package AppBundle\Validators
 * @Annotation
 */
class DayPast extends Constraint
{
    public $message = 'Il n\'est pas possible de visiter le musée lors d\'un jour passer';

    public function validateBy()
    {
        return get_class($this).'Validator';
    }
}